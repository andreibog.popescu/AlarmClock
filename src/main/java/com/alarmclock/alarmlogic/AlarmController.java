/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import com.alarmclock.sound.AlarmSong;
import com.alarmclock.ui.*;

import javax.swing.*;
import java.awt.*;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;

public class AlarmController {

    private static final AlarmController INSTANCE = new AlarmController();
    private final AlarmListView alarmListView = new AlarmListView();
    private final AlarmListScrollView alarmListScrollView = new AlarmListScrollView(
            new Dimension(
                    Integer.parseInt(AlarmProperties.getProperty("ALARMLIST_VIEWPORT_SIZE_X")),
                    Integer.parseInt(AlarmProperties.getProperty("ALARMLIST_VIEWPORT_SIZE_Y"))
            ));
    private final SettingsView settingsCom = new SettingsView();
    private final MenuListView menuListView = new MenuListView();
    private final AlarmButtonView newAlarmButtonView = new AlarmButtonView(AlarmButtonViewType.NEW);
    private final AlarmButtonView settingsButtonView = new AlarmButtonView(AlarmButtonViewType.SETTINGS);
    private final SpringLayout defaultFrameLayout = new SpringLayout();
    private Map<String, AlarmView> viewMap = new HashMap<>();
    private Map<String, AlarmDataModel> modelMap = new HashMap<>();
    private int alarmCount = 0;

    private AlarmController() {
        FrameView.getInstance().addComponent(menuListView);
        FrameView.getInstance().addComponent(alarmListScrollView);
        menuListView.add(newAlarmButtonView);
        menuListView.add(settingsButtonView);
        alarmListScrollView.setViewportView(alarmListView);
        defineButton(newAlarmButtonView, newAlarmButtonView.getType());
        defineButton(settingsButtonView, settingsButtonView.getType());

        setFrameLayout();
        FrameView.getInstance().pack();
    }

    public static AlarmController getController() {
        return INSTANCE;
    }

    public void setAlarmCount(int alarmCount) {
        this.alarmCount = alarmCount;
    }

    public void startNewAlarmThread() {
        AlarmCheckThread alarmCheckThread = new AlarmCheckThread();
        alarmCheckThread.start();
    }

    private void setFrameLayout() {
        defaultFrameLayout.putConstraint(SpringLayout.WEST, menuListView, 5, SpringLayout.WEST, FrameView.getInstance().getContentPane());
        defaultFrameLayout.putConstraint(SpringLayout.NORTH, menuListView, 5, SpringLayout.NORTH, FrameView.getInstance().getContentPane());
        defaultFrameLayout.putConstraint(SpringLayout.WEST, alarmListScrollView, 5, SpringLayout.EAST, menuListView);
        defaultFrameLayout.putConstraint(SpringLayout.NORTH, alarmListScrollView, 5, SpringLayout.NORTH, FrameView.getInstance().getContentPane());
        defaultFrameLayout.putConstraint(SpringLayout.EAST, FrameView.getInstance().getContentPane(), 5, SpringLayout.EAST, alarmListScrollView);
        defaultFrameLayout.putConstraint(SpringLayout.SOUTH, FrameView.getInstance().getContentPane(), 5, SpringLayout.SOUTH, alarmListScrollView);
        FrameView.getInstance().setLayout(defaultFrameLayout);

        newAlarmButtonView.setAlignmentY(Component.TOP_ALIGNMENT);
        settingsButtonView.setAlignmentY(Component.TOP_ALIGNMENT);
    }

    public Map<String, AlarmDataModel> getModelMap() {
        return modelMap;
    }

    public Map<String, AlarmView> getViewMap() {
        return viewMap;
    }

    public void addNewAlarm() {
        AlarmDataModel data = new AlarmDataModel(alarmCount);
        getModelMap().put(Integer.toString(alarmCount), data);
        AlarmDataDBLoader.saveDataToDB(data);
        AlarmView view = new AlarmView(new Dimension(
                Integer.parseInt(AlarmProperties.getProperty("ALARM_PANEL_SIZE_X")),
                Integer.parseInt(AlarmProperties.getProperty("ALARM_PANEL_SIZE_Y"))
        ));
        getViewMap().put(Integer.toString(alarmCount), view);
        view.activateComboBoxes();
        view.setEditable(data.isActive());

        alarmListView.add(view);
        alarmCount++;
    }

    public void loadAlarmDataInView(String key, AlarmDataModel data) {
        getModelMap().put(key, data);
        AlarmView view = new AlarmView(new Dimension(
                Integer.parseInt(AlarmProperties.getProperty("ALARM_PANEL_SIZE_X")),
                Integer.parseInt(AlarmProperties.getProperty("ALARM_PANEL_SIZE_Y"))
        ));
        getViewMap().put(key, view);
        view.activateComboBoxes();
        view.setEditable(data.isActive());

        alarmListView.add(view);
        alarmListView.revalidate();
    }

    public void defineButton(AlarmButtonView alarmButtonView, AlarmButtonViewType type) {
        switch (type) {
            case SETTINGS: {
                alarmButtonView.setText(AlarmProperties.getProperty("ALARM_SETTINGS_TEXT"));
                alarmButtonView.setVisible(true);

                alarmButtonView.addActionListener(e -> {
                    alarmListView.setVisible(alarmButtonView.isPressed());
                    settingsCom.setVisible(!alarmButtonView.isPressed());

                    if (!alarmButtonView.isPressed())
                        alarmListScrollView.setViewportView(settingsCom);
                    else
                        alarmListScrollView.setViewportView(alarmListView);
                    alarmButtonView.setPressed(!alarmButtonView.isPressed());
                });

                break;
            }
            case NEW: {
                alarmButtonView.setText(AlarmProperties.getProperty("ALARM_NEW_TEXT"));
                alarmButtonView.setVisible(true);

                alarmButtonView.addActionListener(e -> {
                    if (!alarmListView.isVisible()) {
                        try {
                            throw new Exception();
                        } catch (Exception exception) {
                            System.out.println("Can't add alarm.");
                        }
                    } else if (alarmListView.getComponentCount() < 20) {
                        addNewAlarm();
                        alarmListView.revalidate();
                    } else
                        System.out.println("Maximum number of alarms reached. Can't add more alarms.");
                });

                break;
            }
            case REMOVE: {
                alarmButtonView.setText(AlarmProperties.getProperty("ALARM_REMOVE_TEXT"));
                alarmButtonView.setVisible(true);

                alarmButtonView.addActionListener(e -> {
                    try {
                        getViewMap()
                                .entrySet()
                                .stream()
                                .filter(entry -> entry.getValue().equals(alarmButtonView.getParent()))
                                .forEach(entry ->
                                {
                                    alarmListView.remove(entry.getValue());
                                    AlarmDataDBLoader.removeDataFromDB(getModelMap().get(entry.getKey()));
                                    getModelMap().remove(entry.getKey());
                                    getViewMap().remove(entry.getKey());
                                    alarmListView.remove(alarmButtonView.getParent());
                                });
                    } catch (ConcurrentModificationException error) {
                        error.getMessage();
                    } finally {
                        alarmListView.revalidate(); //refresh scroll pane
                        alarmListView.validate();   //refresh alarm list
                        alarmListView.repaint();
                    }
                });

                break;
            }
            case EDIT: {
                alarmButtonView.setVisible(true);

                alarmButtonView.addActionListener(e -> getViewMap()
                        .entrySet()
                        .stream()
                        .filter(entry -> entry.getValue().equals(alarmButtonView.getParent()))
                        .forEach(entry ->
                        {
                            getModelMap().get(entry.getKey()).setActive(alarmButtonView.isPressed());
                            AlarmDataDBLoader.saveDataToDB(getModelMap().get(entry.getKey()));
                            entry.getValue().setEditable(alarmButtonView.isPressed());
                            entry.getValue().revalidate();
                        }));

                break;
            }
            case MUTE: {
                alarmButtonView.setText(AlarmProperties.getProperty("ALARM_MUTE_TEXT"));
                alarmButtonView.setEnabled(false);
                alarmButtonView.setVisible(true);

                alarmButtonView.addActionListener(e -> {
                    AlarmSong.getSong().stop();
                    startNewAlarmThread();
                    alarmButtonView.setEnabled(false);
                    alarmButtonView.getParent().revalidate();
                });

                break;
            }
            default:
                break;
        }
    }

    public void setData(String key, int selectedIndex, AlarmDataViewType type) {
        switch (type) {
            case DAYOFTHEWEEK:
                getModelMap().get(key).setDayOfTheWeek(selectedIndex);
                break;
            case HOURS:
                getModelMap().get(key).setHour(selectedIndex);
                break;
            case MINUTES:
                getModelMap().get(key).setMinute(selectedIndex);
                break;
            default:
                break;
        }
        AlarmDataDBLoader.saveDataToDB(getModelMap().get(key));
    }

    public int getData(String key, AlarmDataViewType type) {
        switch (type) {
            case DAYOFTHEWEEK:
                return getModelMap().get(key).getDayOfTheWeek();
            case MINUTES:
                return getModelMap().get(key).getMinute();
            case HOURS:
                return getModelMap().get(key).getHour();
            default:
                return -1;
        }
    }

    public void showFrame() {
        FrameView.getInstance().setVisible(true);
    }
}
