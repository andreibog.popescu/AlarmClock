/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import org.h2.jdbcx.JdbcDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlarmDataDBConnectionProvider {

    private final Logger debugLog = Logger.getLogger(this.getClass().getName());

    /**
     * Connects to database
     *
     * @return true if successful, else false
     */
    public Optional<Connection> getConnection() {
        try {
            debugLog.log(Level.INFO, "Retrieving connection...");
            JdbcDataSource dataSource = new JdbcDataSource();
            StringBuilder connURL = new StringBuilder()
                    .append(AlarmProperties.getProperty("DB_CONNECTION_URL"))
                    .append(" ")
                    .append(AlarmProperties.getProperty("DB_SCHEMA"))
                    .append(";");
            debugLog.log(Level.INFO, "Connection successful.");
            dataSource.setURL(connURL.toString());
            return Optional.of(dataSource.getConnection());
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Failed to connect to database: " + e.getMessage());
            return Optional.empty();
        }
    }
}
