/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Defines load/save interaction with an embedded database
 * Loads/saves alarms
 */
public class AlarmDataDBLoader {

    private static final Logger debugLog = Logger.getLogger(AlarmDataDBLoader.class.getSimpleName());
    private static final AlarmDataDBConnectionProvider connectionProvider = new AlarmDataDBConnectionProvider();

    /**
     * Checks if alarm table exists
     * Create new table if it does exist, else it loads the alarm data from it, then closes the connection
     */
    public static void loadDataFromDB() {
        connectionProvider.getConnection()
                .map(conn -> checkTable(conn))
                .orElseGet(() -> {
                    debugLog.log(Level.SEVERE, "No connection established.");
                    return true;
                });
        connectionProvider.getConnection()
                .map(conn -> loadData(conn))
                .orElseGet(() -> {
                    debugLog.log(Level.SEVERE, "No connection established.");
                    return true;
                });
    }

    public static void saveDataToDB(AlarmDataModel data) {
        connectionProvider.getConnection()
                .map(conn -> saveData(conn, data))
                .orElseGet(() -> {
                    debugLog.log(Level.SEVERE, "No connection established.");
                    return true;
                });
    }

    public static void removeDataFromDB(AlarmDataModel data) {
        connectionProvider.getConnection()
                .map(conn -> removeData(conn, data))
                .orElseGet(() -> {
                    debugLog.log(Level.SEVERE, "No connection established.");
                    return true;
                });
    }

    private static boolean checkTable(Connection dbConnection) {
        try (Connection conn = dbConnection;
             PreparedStatement statement = conn.prepareStatement(defineQuery(QueryType.CREATE))) {
            debugLog.log(Level.INFO, "Checking if table exists...");
            statement.executeUpdate();
            debugLog.log(Level.INFO, "Table check successful.");
            return true;
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Table check failed:" + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private static boolean loadData(Connection dbConnection) {
        try (Connection conn = dbConnection;
             PreparedStatement loadDataStatement = conn.prepareStatement(
                     defineQuery(QueryType.LOAD_ALL),
                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                     ResultSet.CONCUR_READ_ONLY
             );
             ResultSet dataResultSet = loadDataStatement.executeQuery()) {
            debugLog.log(Level.INFO, "Loading data from table...");
            if (dataResultSet.next()) {
                do {
                    AlarmDataModel data = new AlarmDataModel(
                            dataResultSet.getBoolean(AlarmProperties.getProperty("TABLE_COLUMN_ISACTIVE")),
                            dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_DAYOFTHEWEEK")),
                            dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_HOUR")),
                            dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_MINUTE")),
                            dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_ID"))
                    );
                    AlarmController.getController().loadAlarmDataInView(Integer.toString(dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_ID"))), data);
                    //Update count number on last row
                    if (dataResultSet.isLast())
                        AlarmController.getController().setAlarmCount(dataResultSet.getInt(AlarmProperties.getProperty("TABLE_COLUMN_ID")) + 1);
                } while (dataResultSet.next());
                debugLog.log(Level.INFO, "Data loaded successfully!");
                return true;
            } else {
                debugLog.log(Level.WARNING, "No data to load!");
                return false;
            }
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Data load failed:" + e.getMessage());
            e.printStackTrace();
        } finally {
            debugLog.log(Level.INFO, "Connection to database closed.");
        }
        return false;
    }

    /**
     * Saves changed or new data to database
     */
    private static boolean saveData(Connection dbConnection, AlarmDataModel data) {
        try (Connection conn = dbConnection) {
            if (dataExistsInDB(conn, data)) {
                debugLog.log(Level.INFO, "Saving updated alarm data...");
                executeSave(conn, data, QueryType.UPDATE);
            } else {
                debugLog.log(Level.INFO, "Saving new alarm data...");
                executeSave(conn, data, QueryType.INSERT);
            }
            debugLog.log(Level.INFO, "Save successful!");
            return true;
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Query failed: " + e.getMessage());
        } finally {
            debugLog.log(Level.INFO, "Connection to database closed.");
        }
        return false;
    }

    /**
     * Executes save query
     */
    private static void executeSave(Connection dbConnection, AlarmDataModel data, QueryType type) throws SQLException {
        try (PreparedStatement statement = defineStatement(
                dbConnection.prepareStatement(defineQuery(type)),
                type,
                data)) {
            statement.execute();
        }
    }

    /**
     * Deletes record from database
     */
    private static boolean removeData(Connection dbConnection, AlarmDataModel data) {
        try (Connection conn = dbConnection;
             PreparedStatement statement = conn.prepareStatement(
                     defineQuery(QueryType.REMOVE),
                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                     ResultSet.CONCUR_UPDATABLE)
        ) {
            debugLog.log(Level.INFO, "Removing data from database...");
            statement.setInt(1, data.hashCode());
            statement.execute();
            debugLog.log(Level.INFO, "Data removed from database...");
            return true;
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Query failed: " + e.getMessage());
        } finally {
            debugLog.log(Level.INFO, "Connection to database closed.");
        }
        return false;
    }

    /**
     * Check if data exists
     */
    private static boolean dataExistsInDB(Connection dbConnection, AlarmDataModel data) {
        try (PreparedStatement statement = dbConnection.prepareStatement(
                defineQuery(QueryType.SELECT),
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE))
        {
            statement.setInt(1, data.hashCode());
            try(ResultSet resultSet = statement.executeQuery())
            {
                return resultSet.next();
            }
        } catch (SQLException e) {
            debugLog.log(Level.SEVERE, "Query failed: " + e.getMessage());
        }
        return false;
    }

    /**
     * Defines different types of SQL queries
     *
     * @param type of SQL query
     * @return SQL query string
     */
    private static String defineQuery(QueryType type) {
        StringBuilder sqlQuery = new StringBuilder();
        switch (type) {
            case LOAD_ALL:
                sqlQuery.append("SELECT * FROM ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME"));
                break;
            case CREATE:
                sqlQuery.append("CREATE TABLE IF NOT EXISTS ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME"))
                        .append("(").append(AlarmProperties.getProperty("TABLE_COLUMN_ID")).append(" INTEGER not NULL, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ISACTIVE")).append(" BOOLEAN, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_DAYOFTHEWEEK")).append(" INTEGER,")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_HOUR")).append(" INTEGER, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_MINUTE")).append(" INTEGER, ")
                        .append("PRIMARY KEY (").append(AlarmProperties.getProperty("TABLE_COLUMN_ID")).append("))");
                break;
            case UPDATE:
                sqlQuery.append("UPDATE ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME"))
                        .append(" SET ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ISACTIVE")).append(" = ?, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_DAYOFTHEWEEK")).append(" = ?, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_HOUR")).append(" = ?, ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_MINUTE")).append(" = ? WHERE ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ID")).append(" = ?");
                break;
            case INSERT:
                sqlQuery.append("INSERT INTO ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME")).append("(")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ID")).append(", ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ISACTIVE")).append(", ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_DAYOFTHEWEEK")).append(", ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_HOUR")).append(", ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_MINUTE")).append(") ")
                        .append("VALUES(?,?,?,?,?)");
                break;
            case SELECT:
                sqlQuery.append("SELECT * FROM ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME"))
                        .append(" WHERE ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ID"))
                        .append(" = ?");
                break;
            case REMOVE:
                sqlQuery.append("DELETE FROM ")
                        .append(AlarmProperties.getProperty("DB_SCHEMA")).append(".").append(AlarmProperties.getProperty("TABLE_NAME"))
                        .append(" WHERE ")
                        .append(AlarmProperties.getProperty("TABLE_COLUMN_ID"))
                        .append(" = ?");
                break;
            default:
                return null;
        }
        return sqlQuery.toString();
    }

    public static PreparedStatement defineStatement(PreparedStatement statement, QueryType type, AlarmDataModel data) throws SQLException {
        switch (type) {
            case UPDATE:
                statement.setBoolean(1, data.isActive());
                statement.setInt(2, data.getDayOfTheWeek());
                statement.setInt(3, data.getHour());
                statement.setInt(4, data.getMinute());
                statement.setInt(5, data.hashCode());
                return statement;
            case INSERT:
                statement.setInt(1, data.hashCode());
                statement.setBoolean(2, data.isActive());
                statement.setInt(3, data.getDayOfTheWeek());
                statement.setInt(4, data.getHour());
                statement.setInt(5, data.getMinute());
                return statement;
            default:
                return null;
        }
    }

    private enum QueryType {
        CREATE,
        LOAD_ALL,
        SELECT,
        INSERT,
        UPDATE,
        REMOVE
    }
}
