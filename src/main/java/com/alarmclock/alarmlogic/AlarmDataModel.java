/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * The type Alarm data.
 */
public class AlarmDataModel {

    private int alarmNo;
    private boolean isActive;
    private int minute;
    private int hour;
    private int dayOfTheWeek;

    /**
     * Instantiates a new Alarm data.
     */
    public AlarmDataModel(int alarmNo) {
        this.alarmNo = alarmNo;
        isActive = true;
        minute = LocalTime.now().getMinute();
        hour = LocalTime.now().getHour();
        dayOfTheWeek = LocalDate.now().getDayOfWeek().getValue();
    }


    public AlarmDataModel(boolean isActive, int dayOfTheWeek, int hour, int minute, int alarmNo) {
        this.isActive = isActive;
        this.minute = minute;
        this.hour = hour;
        this.dayOfTheWeek = dayOfTheWeek;
        this.alarmNo = alarmNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlarmDataModel that = (AlarmDataModel) o;

        return alarmNo == that.alarmNo;
    }

    @Override
    public int hashCode() {
        return alarmNo;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(int dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }

    @Override
    public String toString() {
        if (hashCode() == -1) return "Invalid AlarmDataModel.";
        return new StringBuilder().append("Data for Alarm ").append(Integer.toString(hashCode())).toString();
    }
}