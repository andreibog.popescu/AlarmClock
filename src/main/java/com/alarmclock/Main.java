/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock;

import com.alarmclock.alarmlogic.AlarmController;
import com.alarmclock.alarmlogic.AlarmDataDBLoader;
import com.alarmclock.alarmlogic.AlarmProperties;

import java.awt.*;

public class Main extends Thread {

    private Main() {
        AlarmProperties.loadAppProperties();
        AlarmProperties.loadUserProperties();
        EventQueue.invokeLater(AlarmController::getController);
        AlarmDataDBLoader.loadDataFromDB();
        AlarmController.getController().startNewAlarmThread();
        AlarmController.getController().showFrame();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        new Main();
    }
}
