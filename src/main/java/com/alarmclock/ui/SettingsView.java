/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.ui;

import javax.swing.*;

public class SettingsView extends JComponent {

    private static final String NO_FILE_SELECTED = "No file selected.";

    private final JLabel soundFileLabel = new JLabel("Soundfile Location: ");
    private final BoxLayout defaultSettingsListLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
    private JTextField soundFileLocation = new JTextField(NO_FILE_SELECTED);

    public SettingsView() {
        setLayout(defaultSettingsListLayout);
        soundFileLabel.setAlignmentX(LEFT_ALIGNMENT);
        soundFileLocation.setAlignmentX(LEFT_ALIGNMENT);
        soundFileLocation.setEditable(false);
        add(soundFileLabel);
        add(soundFileLocation);
    }
}
