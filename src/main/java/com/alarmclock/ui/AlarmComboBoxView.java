package com.alarmclock.ui;

import com.alarmclock.alarmlogic.AlarmController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/*
 *     Created by Popescu Andrei Bogdan on 2/10/2015.
 *     GitHub: https://github.com/andreibogdanpopescu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AlarmComboBoxView extends JComboBox<String> implements ActionListener, MouseWheelListener {


    private static final int MINUTES = 59;
    private static final int HOURS = 23;
    private static final String[] daysOfTheWeek = {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
    };
    private final String[] formattedValues = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09"};
    private AlarmView alarmViewParent;
    private AlarmDataViewType type;

    /**
     * Instantiates a new Alarm combo box.
     *
     * @param type the type
     */
    public AlarmComboBoxView(AlarmDataViewType type) {
        this.type = type;
    }

    private void setSelection() {
        switch (type) {
            case DAYOFTHEWEEK:
                setModel(new DefaultComboBoxModel<>(daysOfTheWeek));
                break;
            case HOURS:
                setModel(new DefaultComboBoxModel<>());
                setAlarmComboBoxData(HOURS);
                break;
            case MINUTES:
                setModel(new DefaultComboBoxModel<>());
                setAlarmComboBoxData(MINUTES);
                break;
            default:
                break;
        }
        setSelectedIndex(AlarmController.getController().getData(alarmViewParent.getViewKey(), type));
    }

    protected void activateComboBox(AlarmView alarmViewParent) {
        this.alarmViewParent = alarmViewParent;
        setSelection();

        addMouseWheelListener(this);
        addActionListener(this);

        setSettings();
    }

    private void setAlarmComboBoxData(int number) {
        for (String count : formattedValues)
            addItem(count);
        for (int count = 10; count <= number; count++)
            addItem(Integer.toString(count));
    }

    private void setSettings() {
        setVisible(true);
        setEditable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setSelectedItem(e.getSource());
        AlarmController.getController().setData(alarmViewParent.getViewKey(), getSelectedIndex(), type);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0) {
            if (getSelectedIndex() == 0)
                setSelectedIndex(getItemCount() - 1);
            else
                setSelectedIndex(getSelectedIndex() - 1);
        } else {
            if (getSelectedIndex() == getItemCount() - 1)
                setSelectedIndex(0);
            else
                setSelectedIndex(getSelectedIndex() + 1);
        }
        AlarmController.getController().setData(alarmViewParent.getViewKey(), getSelectedIndex(), type);
    }
}