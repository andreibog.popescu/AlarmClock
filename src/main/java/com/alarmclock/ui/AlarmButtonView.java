/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.ui;

import javax.swing.*;

public class AlarmButtonView extends JButton {

    private Boolean isPressed = false;
    private AlarmButtonViewType type;

    public AlarmButtonView(AlarmButtonViewType type) {
        this.type = type;
    }

    public AlarmButtonViewType getType() {
        return type;
    }

    public Boolean isPressed() {
        return isPressed;
    }

    public void setPressed(Boolean isPressed) {
        this.isPressed = isPressed;
    }
}
